﻿//Copyright (C) 2017 by Jeroen Vandezande
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.IO;

interface

uses
  System.Collections.Generic,
  System.Text,
  System.IO,
  System.Runtime.Serialization, 
  System.Xml,
  ICSharpCode.SharpZipLib.Core,
  ICSharpCode.SharpZipLib.Zip,
  Newtonsoft.Json;

type

  [DataContract]
  [JsonObject(MemberSerialization.OptIn)]
  SerializableObjectBase = public class
  private
  protected
    class var fSettings := new DataContractSerializerSettings(PreserveObjectReferences := true, IgnoreExtensionDataObject := true, MaxItemsInObjectGraph := Int32.MaxValue);
  public
    [DataMember]
    property Name: String;
    [DataMember]
    property Index: Int32;
    property FileName: String := String.Empty;
    property IsLocalFile: Boolean := false;
  end;

  [DataContract]
  [JsonObject(MemberSerialization.OptIn)]
  SerializableObject<T> = public class(SerializableObjectBase)
    where T is SerializableObject<T>;
  private
    method SaveXMLFile(aFileName: String);
    method SaveCompressedXMLFile(aFileName: String; aPassword: String);
    method SaveJSONFile(aFileName: String);
    method SaveCompressedJSONFile(aFileName: String; aPassword: String);
    [OnDeserialized]
    method OnDeserializedHappened(c: StreamingContext);
    method get_Folder: String;
  protected
  public
    property Folder: String read get_Folder;
    method SaveToFile(aFileName: String; aFileType: SerializableObjectType; aPassword: String);
    method SaveToXMLMemoryStream: MemoryStreamEX;
    method SaveToString(aSerializationType: SerializableObjectType): String;
    method ToString: String; override;
    class method CreateFromFileAsType<FT>(aFileName: String; aPassWord: String): FT; where FT is SerializableObject<T>;
    class method CreateFromFile(aFileName: String; aPassWord: String): T;
    class method CreateFromStream(aStream: Stream): T;
    class method CreateFromString(aString: String): T;
    class method CreateFromStringAsType<FT>(aString: String): FT; where FT is SerializableObject<T>;
    class method CreateFromStream(aStream: Stream; aPassWord: String): T;
    class method CreateFromStreamAsType<FT>(aStream: Stream; aPassWord: String): FT; where FT is SerializableObject<T>;
    class event AfterDeserializing: EventHandler raise;
  end;

  MemoryStreamEX = public class(MemoryStream)
  private
  public
    method Equals(obj: Object): Boolean; override;
  end;

  ObjectDeserializationException = public class(Exception);

  ObjectSerializationException = public class(Exception)
  private
  public
    property FileName: String;
    property UsedFileType: SerializableObjectType;
    property PasswordGiven: Boolean;
    constructor(aFileName: String; aUsedFileType: SerializableObjectType; aPassword: String; aException: Exception);
  end;

  SerializableObjectType = public enum(Xml, CompressedXML, JSON, CompressedJSON);

implementation

method SerializableObject<T>.SaveToFile(aFileName: String; aFileType: SerializableObjectType; aPassword: String);
begin
  try
    self.FileName := aFileName;
    self.IsLocalFile := true;
    self.Name := Path.GetFileNameWithoutExtension(aFileName);
    case aFileType of
      SerializableObjectType.Xml:
      begin
        SaveXMLFile(aFileName);
      end;
      SerializableObjectType.CompressedXML:
      begin
        SaveCompressedXMLFile(aFileName, aPassword);
      end;
      SerializableObjectType.JSON:
      begin
        SaveJSONFile(aFileName);
      end;
      SerializableObjectType.CompressedJSON:
      begin   
        SaveCompressedJSONFile(aFileName, aPassword);
      end;
    end;
  except
    on ex: Exception do
    begin
      raise new ObjectSerializationException(aFileName, aFileType, aPassword, ex);
    end;
  end;
end;

method SerializableObject<T>.SaveXMLFile(aFileName: String);
begin
  var fn := aFileName + ".tmp";
  using ser := new DataContractSerializer(typeOf(self), fSettings) do
  begin
    using writer := File.Create(fn, 4096, FileOptions.WriteThrough) do
    begin
      using xw := XmlWriter.Create(writer, new XmlWriterSettings(Indent := true)) do
      begin
        ser.WriteObject(xw, self);
        writer.Flush();
      end;
    end;
  end;
  if File.Exists(aFileName) then
  begin
    File.Replace(fn, aFileName, nil);
  end
  else 
  begin
    File.Move(fn, aFileName);
  end;
end;

method SerializableObject<T>.SaveCompressedXMLFile(aFileName: String; aPassword: String);
begin
  var fn := aFileName + ".tmp";
  using ser := new DataContractSerializer(typeOf(self), fSettings) do
  begin
    using writer := File.Create(fn, 4096, FileOptions.WriteThrough) do
    begin
      using zipStream := new ZipOutputStream(writer) do
      begin
        if not String.IsNullOrEmpty(aPassword) then zipStream.Password := aPassword;
        zipStream.SetLevel(9);
        zipStream.UseZip64 := UseZip64.Off;
        var newEntry := new ZipEntry(self.Name + '.xml');
        newEntry.DateTime := DateTime.Now;
        zipStream.PutNextEntry(newEntry);
        using xw := XmlWriter.Create(zipStream, new XmlWriterSettings(Indent := true)) do
        begin
          ser.WriteObject(xw, self);
        end;
        zipStream.CloseEntry();
        writer.Flush();
      end;
    end;
  end;
  if File.Exists(aFileName) then
  begin
    File.Replace(fn, aFileName, nil);
  end
  else 
  begin
    File.Move(fn, aFileName);
  end;
end;

method SerializableObject<T>.SaveJSONFile(aFileName: String);
begin
  var fn := aFileName + ".tmp";
  using writer := File.Create(fn, 4096, FileOptions.WriteThrough) do
  begin
    var json := JsonConvert.SerializeObject(self, Formatting.Indented, new JsonSerializerSettings(PreserveReferencesHandling := PreserveReferencesHandling.Objects));
    var jsonByteArray := Encoding.UTF8.GetBytes(json);
    writer.Write(jsonByteArray, 0, jsonByteArray.Length);
    writer.Flush();
  end;
  if File.Exists(aFileName) then
  begin
    File.Replace(fn, aFileName, nil);
  end
  else 
  begin
    File.Move(fn, aFileName);
  end;
end;

method SerializableObject<T>.SaveCompressedJSONFile(aFileName: String; aPassword: String);
begin
  var fn := aFileName + ".tmp";
  using writer := File.Create(fn, 4096, FileOptions.WriteThrough) do
  begin
    using zipStream := new ZipOutputStream(writer) do
    begin
      if not String.IsNullOrEmpty(aPassword) then zipStream.Password := aPassword;
      zipStream.SetLevel(9);
      zipStream.UseZip64 := UseZip64.Off;
      var newEntry := new ZipEntry(self.Name + '.json');
      newEntry.DateTime := DateTime.Now;
      zipStream.PutNextEntry(newEntry);
      var json := JsonConvert.SerializeObject(self, Formatting.Indented, new JsonSerializerSettings(PreserveReferencesHandling := PreserveReferencesHandling.Objects));
      var jsonByteArray := Encoding.UTF8.GetBytes(json);
      zipStream.Write(jsonByteArray, 0, jsonByteArray.Length);
      zipStream.CloseEntry();
      writer.Flush();
    end;
  end;
  if File.Exists(aFileName) then
  begin
    File.Replace(fn, aFileName, nil);
  end
  else 
  begin
    File.Move(fn, aFileName);
  end;
end;

class method SerializableObject<T>.CreateFromFile(aFileName: String; aPassWord: String): T;
begin
  result := CreateFromFileAsType<T>(aFileName, aPassWord); 
end;

method SerializableObject<T>.SaveToXMLMemoryStream: MemoryStreamEX;
begin
  var ser := new DataContractSerializer(typeOf(self), fSettings);
  result := new MemoryStreamEX();
  ser.WriteObject(result, self);
  result.Flush();
end;

class method SerializableObject<T>.CreateFromStream(aStream: Stream): T;
begin
  result := CreateFromStream(aStream, nil);
end;

method SerializableObject<T>.ToString: String;
begin
  result := self.Name;
end;

method SerializableObject<T>.get_Folder: String;
begin
  result := Path.GetDirectoryName(self.FileName);
end;

class method SerializableObject<T>.CreateFromStream(aStream: Stream; aPassWord: String): T;
begin
  result := CreateFromStreamAsType<T>(aStream, aPassWord);
end;

class method SerializableObject<T>.CreateFromFileAsType<FT>(aFileName: String; aPassWord: String): FT;
begin   
  using reader := new FileStream(aFileName, FileMode.Open, FileAccess.Read) do
  begin
    var byteArray := new Byte[8];
    reader.Read(byteArray, 0, 8);
    
    var isZIP := true; 
    var zipSignature := 'PK'+ #3 + #4;   
    for each c in zipSignature index indx do
    begin
      if Byte(c) <> byteArray[indx] then
      begin
        isZIP := false;
        break;
      end;
    end;
      
    reader.Position := 0; //reset reader
    var rawString := String.Empty;
    if isZIP then
    begin
      using zf := new ZipFile(reader) do
      begin
        if not String.IsNullOrEmpty(aPassWord) then zf.Password := aPassWord;
        if zf.Count > 0 then
        begin
          using sr := new StreamReader(zf.GetInputStream(zf[0])) do
          begin
            rawString := sr.ReadToEnd();
          end;
        end;
      end;
    end
    else 
    begin
      using sr := new StreamReader(reader) do
      begin
        rawString := sr.ReadToEnd();
      end;
    end;
    result := CreateFromStringAsType<FT>(rawString); 
    result.IsLocalFile := true;  
    result.FileName := aFileName;
    result.Name := Path.GetFileNameWithoutExtension(aFileName);
  end;
end;

method SerializableObject<T>.SaveToString(aSerializationType: SerializableObjectType): String;
begin
  case aSerializationType of
    SerializableObjectType.Xml:
    begin
      var ser := new DataContractSerializer(typeOf(self), fSettings);
      using tw := new StringWriter() do
        using xw := XmlWriter.Create(tw, new XmlWriterSettings(Encoding := new UTF8Encoding)) do
        begin
          ser.WriteObject(xw, self);
          xw.Flush();
          result := tw.ToString();
        end;
    end;
    SerializableObjectType.JSON:
    begin
      result := JsonConvert.SerializeObject(self, Formatting.Indented, new JsonSerializerSettings(PreserveReferencesHandling := PreserveReferencesHandling.Objects));
    end;
  else
    raise new ArgumentException('Compression is not suported');
  end;
end;

class method SerializableObject<T>.CreateFromString(aString: String): T;
begin
  exit CreateFromStringAsType<T>(aString);  
end;

method SerializableObject<T>.OnDeserializedHappened(c: StreamingContext);
begin
  AfterDeserializing(self, new EventArgs);
end;

class method SerializableObject<T>.CreateFromStringAsType<FT>(aString: String): FT;
begin
  if aString.StartsWith('<') then
  begin //XML data
    using ser := new DataContractSerializer(typeOf(FT), fSettings) do
    begin
      using sr := new System.IO.StringReader(aString) do
      begin
        using xr := XmlReader.Create(sr) do
        begin
          result := ser.ReadObject(xr) as FT;         
        end;
      end;
    end;
  end
  else if aString.StartsWith('{') then
  begin //JSON data
    result := JsonConvert.DeserializeObject<FT>(aString);
  end
  else 
  begin
    raise new ObjectDeserializationException('The received data is neither XML or JSON');
  end;
  result.IsLocalFile := false;  
end;

class method SerializableObject<T>.CreateFromStreamAsType<FT>(aStream: Stream; aPassWord: String): FT;
begin
  aStream.Position := 0;
  var byteArray := new Byte[8];
  aStream.Read(byteArray, 0, 8);
    
  var isZIP := true; 
  var zipSignature := 'PK'+ #3 + #4;   
  for each c in zipSignature index indx do
  begin
    if Byte(c) <> byteArray[indx] then
    begin
      isZIP := false;
      break;
    end;
  end;
      
  aStream.Position := 0; //reset reader
  var rawString := String.Empty;
  if isZIP then
  begin
    using zf := new ZipFile(aStream) do
    begin
      if not String.IsNullOrEmpty(aPassWord) then zf.Password := aPassWord;
      if zf.Count > 0 then
      begin
        using sr := new StreamReader(zf.GetInputStream(zf[0])) do
        begin
          rawString := sr.ReadToEnd();
        end;
      end;
    end;
  end
  else 
  begin
    using sr := new StreamReader(aStream) do
    begin
      rawString := sr.ReadToEnd();
    end;
  end;
  result := CreateFromStringAsType<FT>(rawString); 
end;

method MemoryStreamEX.Equals(obj: Object): Boolean;
begin
  var other := MemoryStreamEX(obj);
  if not assigned(other) then exit false;
  if other.Length <> self.Length then exit false;
  self.Position := 0;
  other.Position := 0;
  for i: Int64 := 0 to self.Length - 1 do
  begin
    if self.ReadByte <> other.ReadByte then exit false;
  end;
  exit true;
end;

constructor ObjectSerializationException(aFileName: String; aUsedFileType: SerializableObjectType; aPassword: String; aException: Exception);
begin
  self.FileName := aFileName;
  self.UsedFileType := aUsedFileType;
  self.PasswordGiven := not String.IsNullOrEmpty(aPassword);
  var serErrorMessage := String.Format("Something went wrong during serialization" + Environment.NewLine +
      "Filename: {0}" + Environment.NewLine + "File Type used: {1}" + Environment.NewLine + "Password given: {2}", FileName, UsedFileType, PasswordGiven);
  if assigned(aException) then
  begin
    serErrorMessage := serErrorMessage + Environment.NewLine + "Cause :" + Environment.NewLine + aException.ToString();
  end;
  inherited constructor(serErrorMessage, aException);
end;


end.