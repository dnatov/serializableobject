using System;
using System.Runtime.Serialization;
using Xunit;
using Essy.IO;
using FluentAssertions;
using System.IO;

namespace SerializableObjectTests
{
    public class SerializeDeserializeTests
    {
        [Fact]
        public void TestClass_DeserializeSerializeCompressedXml_PropertiesOk()
        {
            //Arrange
            var o = new TestClass();
            o.MyProperty1 = 55;
            o.MyProperty2 = "Test";
            o.MyProperty3 = true;
            o.MyProperty4 = 4.88;
            o.MyProperty5 = 44;

            //Act
            var fn = System.IO.Path.GetTempFileName();
            o.SaveToFile(fn, SerializableObjectType.CompressedXML, "test");
            var o2 = TestClass.CreateFromFile(fn, "test");

            //Assert
            o2.MyProperty1.Should().Be(55);
            o2.MyProperty2.Should().Be("Test");
            o2.MyProperty3.Should().Be(true);
            o2.MyProperty4.Should().Be(4.88);
            o2.MyProperty5.Should().Be(0);
        }

        [Fact]
        public void TestClass_DeserializeSerializeCompressedJson_PropertiesOk()
        {
            //Arrange
            var o = new TestClass();
            o.MyProperty1 = 55;
            o.MyProperty2 = "Test";
            o.MyProperty3 = true;
            o.MyProperty4 = 4.88;
            o.MyProperty5 = 44;

            //Act
            var fn = System.IO.Path.GetTempFileName();
            o.SaveToFile(fn, SerializableObjectType.CompressedJSON, "test");
            var o2 = TestClass.CreateFromFile(fn, "test");

            //Assert
            o2.MyProperty1.Should().Be(55);
            o2.MyProperty2.Should().Be("Test");
            o2.MyProperty3.Should().Be(true);
            o2.MyProperty4.Should().Be(4.88);
            o2.MyProperty5.Should().Be(0);
        }

        [Fact]
        public void TestClass_DeserializeSerializeXml_PropertiesOk()
        {
            //Arrange
            var o = new TestClass();
            o.MyProperty1 = 55;
            o.MyProperty2 = "Test";
            o.MyProperty3 = true;
            o.MyProperty4 = 4.88;
            o.MyProperty5 = 44;

            //Act
            var fn = System.IO.Path.GetTempFileName();
            o.SaveToFile(fn, SerializableObjectType.Xml, "test");
            var o2 = TestClass.CreateFromFile(fn, "test");

            //Assert
            o2.MyProperty1.Should().Be(55);
            o2.MyProperty2.Should().Be("Test");
            o2.MyProperty3.Should().Be(true);
            o2.MyProperty4.Should().Be(4.88);
            o2.MyProperty5.Should().Be(0);
        }

        [Fact]
        public void TestClass_DeserializeSerializeJson_PropertiesOk()
        {
            //Arrange
            var o = new TestClass();
            o.MyProperty1 = 55;
            o.MyProperty2 = "Test";
            o.MyProperty3 = true;
            o.MyProperty4 = 4.88;
            o.MyProperty5 = 44;

            //Act
            var fn = System.IO.Path.GetTempFileName();
            o.SaveToFile(fn, SerializableObjectType.JSON, "test");
            var o2 = TestClass.CreateFromFile(fn, "test");

            //Assert
            o2.MyProperty1.Should().Be(55);
            o2.MyProperty2.Should().Be("Test");
            o2.MyProperty3.Should().Be(true);
            o2.MyProperty4.Should().Be(4.88);
            o2.MyProperty5.Should().Be(0);
        }
    }


    [DataContract]
    class TestClass: SerializableObject<TestClass>
    {
        [DataMember]
        public int MyProperty1 { get; set; }
        [DataMember]
        public String MyProperty2 { get; set; }
        [DataMember]
        public Boolean MyProperty3 { get; set; }
        [DataMember]
        public Double MyProperty4 { get; set; }
        public int MyProperty5 { get; set; }
        public TestClass()
        {

        }
    }
}
