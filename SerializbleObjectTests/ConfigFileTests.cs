﻿using System;
using System.Runtime.Serialization;
using Xunit;
using Essy.IO;
using FluentAssertions;
using System.IO;
using System.Reflection;

namespace SerializbleObjectTests
{
    public class ConfigFileTests
    {
        [Fact]
        public void TestClass_SerializeCorruptXmlFile_ErrorOccuredDefaultConfigReturnedInvoked()
        {
            //Arrange
            var fileNameRestoredToDefault = "";
            
            //Create config
            TestConfig.Data.IsTest = true;
            TestConfig.Data.Save();
            var testBool = TestConfig.Data.IsTest;
            
            //Get config path
            var filePath = TestConfig.Data.FileName;
            
            //Hook event
            AppConfig<TestConfig>.ErrorOccuredDefaultConfigReturned += delegate (object sender, DefaultReturnedEventArgs e)
            {
                fileNameRestoredToDefault = e.FileName;
            };
            
            //Remove object memory to simulate application start
            TestConfig.Data = null;
            
            //Simulate corrupt config file, including backup
            CorruptFile(filePath);
            CorruptFile(filePath + "~");

            //Act
            var testBool2 = TestConfig.Data.IsTest;

            //Assert
            File.Exists(filePath + ".corrupt").Should().BeTrue();
            fileNameRestoredToDefault.Should().NotBeNullOrEmpty();
            testBool.Should().BeTrue();
            testBool2.Should().BeFalse();

            //Cleanup
            Directory.Delete(Path.GetDirectoryName(filePath), true);
        }

        public void CorruptFile(string path)
        {
            using (var stream = new FileStream(path, FileMode.Open))
            {
                var garbage = new byte[] { 0x0, 0x0, 0x0, 0x0, 0x0 };
                stream.Position = 0;
                stream.Write(garbage, 0, 5);
            }
        }
    }

    [ConfigFileName("Test_Configuration", "TestConfig", SerializableObjectType.Xml, true, true)]
    [DataContract]
    internal sealed class TestConfig: AppConfig<TestConfig>
    {
        [DataMember]
        public bool IsTest;
        [DataMember]
        public int AnInteger = 8;
        [DataMember]
        public string SomeString = "Test";
    }
}
